"use strict";

const gulp = require("gulp");
const browserSync = require('browser-sync').create();
const plumber = require('gulp-plumber');
const rigger = require('gulp-rigger');
const sourcemaps = require('gulp-sourcemaps');
const sass = require('gulp-sass');
const autoprefixer = require('autoprefixer');
const uglify = require('gulp-uglify');
const cache = require('gulp-cache');
const imagemin = require('gulp-imagemin');
const jpegrecompress = require('imagemin-jpeg-recompress');
const pngquant = require('imagemin-pngquant');
const del = require('del');
const postcss = require('gulp-postcss');

const path = {
    dist: {
        html: 'dist/',
        js: 'dist/js/',
        jsmain: 'dist/js',
        css: 'dist/css/',
        img: 'dist/img/',
        fonts: 'dist/fonts/',
        otherprojects: 'dist/'
    },
    app: {
        html: 'app/*.*',
        js: 'app/js/*.js',
        jsmain: 'app/js/*.js',
        sass: 'app/sass/*.sass',
        css: 'app/css/',
        img: 'app/img/**/*.*',
        fonts: 'app/fonts/**/*.*',
        otherprojects: 'app/otherprojects/**/*.*'
    },
    watch: {
        html: 'app/*.html',
        htmlTemplate: 'app/template/*.html',
        js: 'app/js/*.js',
        jsmain: 'app/js/*.js',
        sass: 'app/sass/*.sass',
        css: 'app/css/*.css',
        img: 'app/img/**/*.*',
        fonts: 'app/fonts/**/*.*'
    },
    clean: './dist/'
};

const config = {
    server: {
        baseDir: './dist'
    },
};

gulp.task('otherprojects', function(done) {
    gulp.src(path.app.otherprojects)
        .pipe(gulp.dest(path.dist.otherprojects))
        .pipe(browserSync.reload({ stream: true }));
    done();
});

gulp.task('browserSync', function(done) {
    browserSync.init(config);
    done();
});


gulp.task('html:build', function(done) {
    gulp.src(path.app.html)
        .pipe(plumber())
        .pipe(rigger())
        .pipe(gulp.dest(path.dist.html))
        .pipe(browserSync.reload({ stream: true }));
    done();
});


gulp.task('sass:build', function(done) {
    gulp.src(path.app.sass)
        .pipe(plumber())
        .pipe(sourcemaps.init())
        .pipe(sass({outputStyle: 'compressed'}).on('error', sass.logError))
        .pipe(postcss([autoprefixer({ overrideBrowserslist: ['last 10 version'] })]))
        .pipe(sourcemaps.write('./'))
        .pipe(gulp.dest(path.app.css))
        .pipe(gulp.dest(path.dist.css))
        .pipe(browserSync.stream({once: true}));
    done();
});


gulp.task('jsmain:build', function(done) {
    gulp.src(path.app.jsmain)
        .pipe(plumber())
        .pipe(rigger())
        .pipe(sourcemaps.init())
        .pipe(uglify())
        .pipe(sourcemaps.write('./'))
        .pipe(gulp.dest(path.dist.jsmain))
        .pipe(browserSync.reload({ stream: true }));
    done();
});


gulp.task('js:build', function(done) {
    gulp.src([path.app.js, '!app/js/main.js'])
        .pipe(gulp.dest(path.dist.js));
    done();
});


gulp.task('fonts:build', function(done) {
    gulp.src(path.app.fonts)
        .pipe(gulp.dest(path.dist.fonts));
    done();
});


gulp.task('image:build', function(done) {
    gulp.src(path.app.img)
        // .pipe(cache(imagemin([
        //     imagemin.gifsicle({ interlaced: true }),
        //     jpegrecompress({
        //         progressive: true,
        //         max: 90,
        //         min: 80
        //     }),
        //     pngquant(),
        //     imagemin.svgo({ plugins: [{ removeViewBox: false }] })
        // ])))
        .pipe(gulp.dest(path.dist.img));
    done();
});


gulp.task('clean:build', function(done) {
    del.sync(path.clean);
    done();
});


gulp.task('cache:clear', function(done) {
    cache.clearAll();
    done();
});


gulp.task('build',
    gulp.series('clean:build', 'html:build', 'sass:build', 'js:build', 'jsmain:build', 'fonts:build', 'image:build', 'otherprojects', function(done) { //'css:build',
    done();
}));



gulp.task('watch', function() {
    gulp.watch(path.watch.html, gulp.series('html:build'));
    gulp.watch(path.watch.htmlTemplate, gulp.series('html:build'));
    gulp.watch(path.watch.sass, gulp.series('sass:build'));
    gulp.watch(path.watch.js, gulp.series('js:build'));
    gulp.watch(path.watch.img, gulp.series('image:build'));
    gulp.watch(path.watch.fonts, gulp.series('fonts:build'));
    gulp.watch(path.watch.jsmain, gulp.series('jsmain:build'));
});


gulp.task('default', gulp.series('clean:build', 'build', gulp.parallel('browserSync', 'watch')));
